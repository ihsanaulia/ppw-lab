from django.apps import AppConfig


class MypersonalwebsiteConfig(AppConfig):
    name = 'myPersonalWebsite'
