from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.

class Schedule(models.Model):
    kegiatan = models.CharField(max_length=20)
    waktu = models.DateTimeField()
    tempat = models.CharField(max_length=20)
    kategori = models.CharField(max_length=20)

    def __str__(self):
        return self.kegiatan
