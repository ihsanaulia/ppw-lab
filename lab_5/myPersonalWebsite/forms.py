from django import forms
from .models import *
class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'
class PostSchedule(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    kegiatan = forms.CharField(label = 'Aktivitas', required = True, widget=forms.TextInput(attrs=attrs), max_length=20)
    waktu = forms.DateTimeField(label = 'Waktu', required = True, widget=DateTimeInput(attrs=attrs), input_formats = '%m/%d/%Y %H:%M')
    tempat = forms.CharField(label = 'Lokasi', required = True, widget=forms.TextInput(attrs=attrs), max_length=20)
    kategori = forms.CharField(label = 'Kategori', required = True, widget=forms.TextInput(attrs=attrs), max_length=20)

    class Meta:
        model = Schedule
