from django.urls import re_path, path
from .views import index,forms,skills,experiences,educations, schedule_list
from . import views
#url for app
urlpatterns = [
    re_path(r'personalWeb/', index, name='index'),
    re_path(r'forms/', forms, name='forms'),
	re_path(r'skills/', skills, name='skills'),
    re_path(r'experiences/', experiences, name='experiences'),
	re_path(r'educations/', educations, name='educations'),
    re_path(r'schedule_list/', schedule_list, name='schedule_list'),
]
