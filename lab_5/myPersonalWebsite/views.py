from django.shortcuts import render
from .models import Schedule
from .forms import *
from django.http import HttpResponseRedirect
from django.shortcuts import render
response = {'author': "Ihsan"}
def index(request):
	return render(request, 'personalWeb.html', {})
def forms(request):
	response['schedule_form'] = PostSchedule
	return render(request, 'forms.html', response)
def skills(request):
	return render(request, 'skills.html', {})
def experiences(request):
	return render(request, 'experiences.html', {})
def educations(request):
	return render(request, 'educations.html', {})
def schedule_list(request):
	form = PostSchedule(request.POST or None)
	if (request.method == 'POST' and 'submit' in request.POST):
		response['kegiatan'] = request.POST['kegiatan']if request.POST['kegiatan']!="" else "Anonymus"
		response['waktu'] = request.POST['waktu']if request.POST['waktu']!="" else "Anonymus"
		response['tempat'] = request.POST['tempat']if request.POST['tempat']!="" else "Anonymus"
		response['kategori'] = request.POST['kategori']
		postschedule = Schedule(kegiatan=response['kegiatan'], waktu=response['waktu'], tempat=response['tempat'], kategori=response['kategori'])
		postschedule.save()
		allschedule = Schedule.objects.all()
		response['schedule'] = allschedule
		html = 'schedule_list.html'
		return render(request, html, response)
	elif (request.method == 'POST' and 'delete' in request.POST):
		html = 'schedule_list.html'
		allschedule = Schedule.objects.all()
		response['schedule'] = allschedule.delete()
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/forms/')

# Create your views here.
