from django.urls import re_path, path
from .views import index,form
#url for app
urlpatterns = [
    re_path(r'personalWeb/', index, name='index'),
    re_path(r'form/', form, name='form'),
]
