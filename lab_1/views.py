from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ihsan Muhammad Aulia' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 11, 27) #TODO Implement this, format (Year, Month, Date)
univ = 'Universitas Indonesia'
npm = 1706043393 # TODO Implement this
hobby = 'Reading'
bio = 'I love eating and sleeping'

frname =  'Ramawajdi Kanishka Anwar'
frbirth_1 = date(1999, 1, 12)
fruniv = 'Universitas Indonesia'
frnpm = 1706043595
frhobby = 'Isi bensin'
frbio = 'Halo! Nama saya Ramawajdi Kanishka Anwar. Biasanya dipanggil kani. Diantara teman-teman saya, saya paling suka isi bensin. Baik itu untuk kendaraan saya, ataupun bensin untuk diri sendiri. Saya tinggal di Bekasi. Perbedaan umur antara saya dengan kakak saya adalah 8 tahun. Selain mengisi bensin, saya juga suka menyanyi dan memasak. Salah satu quote favorit saya adalah “Wit and puns arent just decor in the mind; theyre essential signs that the mind knows its on, recognizes its own software, can spot the bugs in its own program.” ― Adam Gopnik.'

frname2 = 'Michael Christopher Manullang'
frbirth_2 = date(1999, 1, 2)
fruniv2 = 'Universitas Indonesia'
frnpm2 = 1706039723
frhobby2 = 'Studying'
frbio2 = 'I like studying'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'university': univ, 'npm': npm,  'age': calculate_age(birth_date.year), 'hobby': hobby, 'bio': bio, 
				'frname': frname, 'fruniv': fruniv, 'frnpm': frnpm,  'frage': calculate_age(frbirth_1.year), 'frhobby': frhobby, 'frbio': frbio, 
				'frname2': frname2, 'fruniv2': fruniv2, 'frnpm2': frnpm2,  'frage2': calculate_age(frbirth_2.year), 'frhobby2': frhobby2, 'frbio2': frbio2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
 
