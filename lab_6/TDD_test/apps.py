from django.apps import AppConfig


class TddTestConfig(AppConfig):
    name = 'TDD_test'
