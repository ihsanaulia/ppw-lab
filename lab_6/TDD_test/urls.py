from django.urls import path, re_path
from .views import index
from .views import post_status
#this is a comment
urlpatterns = [
    path('', index, name='index'),
    re_path('test/', post_status, name='post_status')
]
