from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client
from .forms import StatusForm
from .models import Status
from .views import index
# Create your tests here.

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class HomePageTests(TestCase):

    def setUp(self):
        return Status.objects.create(title='haduh', status='stress ppw')

    def test_home_page_status_code(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_view_renders_correct_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, 'home.html')

    def test_index_func(self):
        res = resolve('/')
        self.assertEqual(res.func, index)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello, Apa kabar?", html_response)

    def test_newspage_news_can_print(self):
        new_status = self.setUp()
        self.assertEqual('haduh', new_status.__str__())

    def test_model_can_create_new_status(self):
        new_status = self.setUp()
        self.assertEqual(new_status.title, 'haduh')
        self.assertEqual(new_status.status, 'stress ppw')

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'title': '', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    def test_lab_post_failed(self):
        response = Client().post('/test/', {'title': '', 'status': 'test'})
        self.assertEqual(response.status_code, 302)

    def test_form_placeholder(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_title">Title:</label>', form.as_p())
        self.assertIn('<label for="id_status">Status:</label>', form.as_p())

    def test_post_succeded_and_render(self):
        new_status = self.setUp()
        response_post = Client().post('/test/', {'title': new_status.title, 'status': new_status.status})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(new_status.title, html_response)
        self.assertIn(new_status.status, html_response)


class PageFunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(PageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(PageFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium

        # Opening the page
        selenium.get('http://127.0.0.1:8000/')

        # find the form element
        title = selenium.find_element_by_name('title')
        description = selenium.find_element_by_name('status')

        submit = selenium.find_element_by_name('submit')

        # filling the form
        title.send_keys('Mengerjakan Lab PPW')
        description.send_keys('Lab kali ini membahas tentang functional test')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        # find the input and check it
        self.assertIn('Mengerjakan Lab PPW', self.selenium.page_source)
        self.assertIn('Lab kali ini membahas tentang functional test', self.selenium.page_source)

    def test_if_page_has_greetings(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        assert selenium.page_source.find("Hello, Apa kabar?")

    def test_page_has_footer(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        footer = selenium.find_element_by_class_name('footer').text
        self.assertIn("Ihsan Muhammad Aulia", footer)

    def test_footer_in_the_right_position(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        footer = selenium.find_element_by_class_name('footer').value_of_css_property("position")
        self.assertEqual('absolute', footer)

    def test_footer_has_the_right_colour(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        footer = selenium.find_element_by_class_name('footer').value_of_css_property("color")
        self.assertEquals('rgba(33, 37, 41, 1)', footer)

    def test_footer_text_has_the_right_colour(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        footer_text = selenium.find_element_by_class_name('text-muted').value_of_css_property("color")
        self.assertEquals('rgba(108, 117, 125, 1)', footer_text)

