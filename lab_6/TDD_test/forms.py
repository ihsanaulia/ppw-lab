from django import forms
from .models import *

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    title = forms.CharField(label = 'Title', required = True, widget=forms.TextInput(attrs=attrs), max_length=30)
    status = forms.CharField(label = 'Status', required = True, widget=forms.TextInput(attrs=attrs), max_length=300)

    class Meta:
        model = Status
