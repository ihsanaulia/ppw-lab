from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
response = {}

def index(request):
    allstatus = Status.objects.all().order_by('-created_date')
    response['all_status'] = allstatus
    response['status_form'] = StatusForm
    return render(request, "home.html", response)

def post_status(request):
    if request.method == 'POST' and 'submit' in request.POST:
        response['title'] = request.POST['title']
        response['status'] = request.POST['status']
        status_save = Status(title=response['title'], status=response['status'])
        status_save.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')